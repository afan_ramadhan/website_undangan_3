<?php
include "config.php";
?>

<!DOCTYPE html>

<html class="no-js">

	<head>
	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<title><?=$title;?></title>
		
		<meta name="keywords" content="<?=$keywords;?>">
		<meta name="description" content="<?=$description;?>">
		<meta name="author" content="<?=$author;?>">
		
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		
		<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
		<link rel="apple-touch-icon" href="images/favicon.png">

		<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
		<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="css/icomoon.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/owl.carousel.min.css">
		<link rel="stylesheet" href="css/owl.theme.default.min.css">
		<link rel="stylesheet" href="css/style.css">
		
		<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css"/>
		
		<script src="js/modernizr-2.6.2.min.js"></script>
		
		<style>
			.custom-font-1 {
				font-family: 'Sacramento', Arial, serif;
				font-size: 25px;
			}
			
			.custom-font-2 {
				font-weight: bold;
				font-size: 15px;
			}
			
			#toggle-button {
				position: fixed;
				bottom: 17px;
				left: 20px;
				z-index: 9999;
			}
			
			.pesan {
				margin-top: 10px;
				font-size: 20px;
			}
			
			.tanggal-waktu-pesan {
				font-size: 14px;
			}
			
			.social-media li {
				display: inline;
				margin-right: 20px;
			}
			
			#fh5co-started select option {
				color: #333333;
			}
		</style>
		
	</head>
	
	<body>
		
		<div class="fh5co-loader"></div>
		
		<div id="page">
		
			<nav class="fh5co-nav" role="navigation">
				<div class="container">
					<div class="row">
						<div class="col-xs-4">
							<?=($mobile == false ? "<div id='fh5co-logo'><a href='index.php'>$pengantin_1 & $pengantin_2</a></div>" : "");?>
						</div>
						<div class="col-xs-8 text-right menu-1">
							<ul>
								<li><a href="#fh5co-header">Beranda</a></li>
								<li><a href="#fh5co-couple">Profil Pengantin</a></li>
								<li><a href="#fh5co-event">Acara</a></li>
								<li><a href="#fh5co-services">Lokasi</a></li>
								<li><a href="#fh5co-couple-story">Protokol Kesehatan</a></li>
								<li><a href="#fh5co-started">Ucapan Dan Do'a</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</nav>


			<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(images/img_bg_1.jpg);" data-stellar-background-ratio="0.5">
				<div class="overlay"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center">
							<div class="display-t">
								<div class="display-tc animate-box" data-animate-effect="fadeIn">
									<h1><?=$pengantin_1;?> & <?=$pengantin_2;?></h1>
									<h2>
										<?=$sambutan_undangan;?>
										<br/>
										<span style="color:#FF9900"><?=$tanggal;?></span>
									</h2>
									<div class="simply-countdown simply-countdown-one"></div>
									<p><a href="#fh5co-event" class="btn btn-default btn-sm">Save the date</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>


			<div id="fh5co-couple">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h2><?=$pengantin_1;?> & <?=$pengantin_2;?></h2>
						</div>
					</div>
					<div class="couple-wrap animate-box">
						<div class="couple-half">
							<div class="groom">
								<img src="images/groom.jpg" alt="groom" class="img-responsive">
							</div>
							<div class="desc-groom">
								<h3><?=$pengantin_1;?></h3>
								<p><?=$profil_pengantin_1;?></p>
								<ul class="social-media">
									<?=($facebook_pengantin_1 == "" ? "" : "<li><a href='$facebook_pengantin_1'><i class='fa fa-facebook fa-2x'></i></a></li>");?>
									<?=($twitter_pengantin_1 == "" ? "" : "<li><a href='$twitter_pengantin_1'><i class='fa fa-twitter fa-2x'></i></a></li>");?>
									<?=($instagram_pengantin_1 == "" ? "" : "<li><a href='$instagram_pengantin_1'><i class='fa fa-instagram fa-2x'></i></a></li>");?>
								</ul>
							</div>
						</div>
						<p class="heart text-center"><i class="icon-heart2"></i></p>
						<div class="couple-half">
							<div class="bride">
								<img src="images/bride.jpg" alt="groom" class="img-responsive">
							</div>
							<div class="desc-bride">
								<h3><?=$pengantin_2;?></h3>
								<p><?=$profil_pengantin_2;?></p>
								<ul class="social-media">
									<?=($facebook_pengantin_2 == "" ? "" : "<li><a href='$facebook_pengantin_2'><i class='fa fa-facebook fa-2x'></i></a></li>");?>
									<?=($twitter_pengantin_2 == "" ? "" : "<li><a href='$twitter_pengantin_2'><i class='fa fa-twitter fa-2x'></i></a></li>");?>
									<?=($instagram_pengantin_2 == "" ? "" : "<li><a href='$instagram_pengantin_2'><i class='fa fa-instagram fa-2x'></i></a></li>");?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div id="fh5co-event" class="fh5co-bg" style="background-image:url(images/img_bg_3.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h2>Acara</h2>
							<span><?=$acara_ket;?></span>
						</div>
					</div>
					<div class="row">
						<div class="display-t">
							<div class="display-tc">
								<div class="col-md-10 col-md-offset-1">
									<div class="col-md-6 col-sm-6 text-center">
										<div class="event-wrap animate-box">
											<h3><?=$acara_1_ket;?></h3>
											<div class="event-col">
												<i class="icon-calendar"></i>
												<span><?=$tanggal_acara_1;?></span>
											</div>
											<div class="event-col">
												<i class="icon-clock"></i>
												<span><?=$waktu_acara_1;?></span>
											</div>
											<p><?=$lokasi_acara_1;?></p>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 text-center">
										<div class="event-wrap animate-box">
											<h3><?=$acara_2_ket;?></h3>
											<div class="event-col">
												<i class="icon-calendar"></i>
												<span><?=$tanggal_acara_2;?></span>
											</div>
											<div class="event-col">
												<i class="icon-clock"></i>
												<span><?=$waktu_acara_2;?></span>
											</div>
											<p><?=$lokasi_acara_2;?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div id="fh5co-services" class="fh5co-section-gray">
				<div class="container">
					
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2>Lokasi</h2>
							<p><?=$lokasi;?></p>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 animate-box text-center">
							<iframe src="<?=$lokasi_src;?>" width="100%" height="450" style="border: 0; margin-bottom: 20px;" allowfullscreen="" loading="lazy"></iframe>
							<p><a href="<?=$lokasi_href;?>" class="btn btn-default btn-sm">Buka Di Google Maps <i class="fa fa-map-marker" aria-hidden="true"></i></a></p>
						</div>
					</div>

					
				</div>
			</div>


			<div id="fh5co-couple-story">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
							<h2>Protokol Kesehatan</h2>
							<span>Mohon Untuk Para Tamu Undangan Untuk Tetap Memperhatikan Pprotokol Kesehatan Yaitu :</span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-md-offset-0">
							<ul class="timeline animate-box">
								<li class="animate-box">
									<div class="timeline-badge" style="background-image:url(images/protokol-kesehatan-01.jpg);"></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h3 class="timeline-title">1. Menggunakan Masker</h3>
										</div>
										<div class="timeline-body">
											<p>Selalu/wajib menggunakan masker baik di dalam maupun luar ruangan.</p>
										</div>
									</div>
								</li>
								<li class="timeline-inverted animate-box">
									<div class="timeline-badge" style="background-image:url(images/protokol-kesehatan-02.jpg);"></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h3 class="timeline-title">2. Mencuci Tangan</h3>
										</div>
										<div class="timeline-body">
											<p>Cuci tangan secara rutin. Gunakan air dan sabun/<i>hand sanitizer</i>.</p>
										</div>
									</div>
								</li>
								<li class="animate-box">
									<div class="timeline-badge" style="background-image:url(images/protokol-kesehatan-03.jpg);"></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h3 class="timeline-title">3. Menjaga Jarak</h3>
										</div>
										<div class="timeline-body">
											<p>Menjaga jarak minimal 1 meter dengan orang lain untuk menghindari terkena <i>droplets</i> dari orang yang bicara, batuk, atau bersin.<br/></p>
										</div>
									</div>
								</li>
								<li class="timeline-inverted animate-box">
									<div class="timeline-badge" style="background-image:url(images/protokol-kesehatan-04.jpg);"></div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											<h3 class="timeline-title">4. Menghindari Kerumunan</h3>
										</div>
										<div class="timeline-body">
											<p>Hindari dan jangan bikin kerumunan. Semakin banyak dan sering kamu bertemu orang, maka kemungkinan terinfeksi virus corona pun semakin tinggi.<br/></p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>


			<div id="fh5co-started" class="fh5co-bg" style="background-image:url(images/img_bg_4.jpg);">
				<div class="overlay"></div>
				<div class="container">
					<div class="row animate-box">
						<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
							<h2>Ucapan Dan Do'a</h2>
							<p><?=$doa_ket;?></p>
						</div>
					</div>
					<div class="row animate-box">
											<div class="col-lg-12 col-sm-12 col-xs-12">
					  <div class="contact-block">
						<form id="contactForm">
						  <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" maxlength="75" placeholder="Nama Lengkap" required data-error="Silahkan Masukan Nama Lengkap">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="alamat" name="alamat" maxlength="200" placeholder="Alamat" required data-error="Silahkan Masukan Alamat">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="telepon" name="telepon" maxlength="15" placeholder="Nomor Telepon (Opsional)">
									<div class="help-block with-errors"></div>
								</div>                                 
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<select class="custom-select d-block form-control" id="kehadiran" required data-error="Silahkan Pilih Opsi Kehadiran.">
										<option value="" disabled selected>Pilih Kehadiran :</option>
										<option value="1">Insyaallah Hadir</option>
										<option value="0">Maaf Belum Bisa Hadir</option>
									</select>
									<div class="help-block with-errors"></div>
								</div> 
							</div>
							<div class="col-md-12">
								<div class="form-group"> 
									<textarea class="form-control" id="pesan" placeholder="Berikan Ucapan Dan Do'a.. ." rows="8" data-error="Silahkan Berikan Ucapan Dan Do'a" required></textarea>
									<div class="help-block with-errors"></div>
								</div>
								<div class="submit-button text-center">
									<button class="btn btn-common" id="submit" type="submit">Kirim</button>
									<div id="msgSubmit" class="h3 text-center hidden" style="margin-top: 20px;"></div> 
									<div class="clearfix"></div> 
								</div>
							</div>
						  </div>            
						</form>
					  </div>
					</div>
					</div>
				</div>
			</div>


			<div id="fh5co-testimonial">
				<div class="container">
					<div class="row">
						<div class="row animate-box">
							<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
								<h2>Terima Kasih Atas Do'anya</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 animate-box">
								<div class="wrap-testimony">
									<div class="panel panel-default widget">
										<div class="panel-body" style="max-height: 500px; overflow-x: auto;">
											<ul class="list-group">
												
												<?php
												$dataPesan = mysql_query("SELECT * FROM pesan ORDER BY id_pesan DESC");
												while($ambilDataPesan = mysql_fetch_array($dataPesan))
												{
												?>
												
													<li class="list-group-item">
														<div class="row">
															<div class="col-xs-12 col-md-12">
																<div class="pengirim">
																	<a href="#doa-pesan"><?=$ambilDataPesan['nama_lengkap'];?></a>
																	<div class="mic-info">
																		Di: <a href="#doa-pesan"><?=$ambilDataPesan['alamat'];?></a>
																	</div>
																</div>
																<div class="pesan">
																	<i>"<?=$ambilDataPesan['pesan'];?>"</i>
																</div>
																<div class="tanggal-waktu-pesan">
																	<i class="fa fa-calendar-o" aria-hidden="true" style="margin-right: 10px;"></i><?=tanggal_indonesia($ambilDataPesan['tanggal_dikirim']);?>
																</div>
															</div>
														</div>
													</li>
												
												<?php
												}
												?>
												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="modal fade" id="sambutan" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title custom-font-1">Assalamu'alaikum Warohmatullahi Wabarokatuh.. .</h4>
						</div>
						<div class="modal-body">
							<table>
								<tr>
									<td class="custom-font-2" align="right">Teruntuk</td>
									<td class="custom-font-2" style="padding-left: 10px; padding-right: 10px;">:</td>
									<td class="custom-font-2"><?=$penerima;?></td>
								</tr>
								<tr>
									<td class="custom-font-2" align="right">Di</td>
									<td class="custom-font-2" style="padding-left: 10px; padding-right: 10px;">:</td>
									<td class="custom-font-2"><?=$alamat;?></td>
								</tr>
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-sm" data-dismiss="modal" onclick="playAudio()">Tutup</button>
						</div>
					</div>
				</div>
			</div>
			
			
			<audio id="opening" controls style="display: none;">
				<source src="audio/murrotal.mp3" type="audio/mpeg">
				Your browser does not support the audio element.
			</audio>
			
			
			<div id="toggle-button">
				<button class="btn btn-warning" style="border-radius: 4px; width: 50px; height: 50px; padding: 0px;" onclick="toggleAudio()"><i id="toggle-icon" class="fa fa-pause" aria-hidden="true"></i></button>
			</div>
			

			<footer id="fh5co-footer" role="contentinfo">
				<div class="container">
					<div class="row copyright">
						<div class="col-md-12 text-center">
							<p>
								<small class="block">Mau Bikin Website Undangan Juga? Silahkan Pesan Di : <a href="https://wa.me/6289609918353">Ketanware</a></small>
							</p>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<div class="gototop js-top">
			<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
		</div>
		
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.waypoints.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/jquery.countTo.js"></script>
		<script src="js/jquery.stellar.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/magnific-popup-options.js"></script>
		<script src="js/simplyCountdown.js"></script>
		<script src="js/main.js"></script>
		<script src="js/form-validator.min.js"></script>
		<script src="js/contact-form-script.js"></script>

		<script>
			$('.simply-countdown-one').simplyCountdown({
				year: 2021,
                month: 12,
                day: 1,
                hours: 10,
                minutes: 0,
                seconds: 0,
				words: {
                    days: 'hari',
                    hours: 'jam',
                    minutes: 'menit',
                    seconds: 'detik',
                    pluralLetter: 's'
                },
				plural: false,
                inline: false,
                enableUtc: false,
			});
			
			$.when($(".fh5co-loader").fadeOut("slow")).done(function(){
				$("#sambutan").modal();
			});
			
			var audio = document.getElementById("opening"); 
			
			function playAudio(){
				audio.play();
			}
			
			function toggleAudio(){
				var cekStatus = audio.paused;
				
				if(cekStatus == false)
				{
					audio.pause();
					$("#toggle-icon").removeClass("fa-pause");
					$("#toggle-icon").addClass("fa-play");
				}
				else
				{
					audio.play();
					$("#toggle-icon").removeClass("fa-play");
					$("#toggle-icon").addClass("fa-pause");
				}
			}
			
			document.getElementById("opening").addEventListener("ended", myHandler, false);
			
			function myHandler(e){
				$("#toggle-icon").removeClass("fa-pause");
				$("#toggle-icon").addClass("fa-play");
			}
		</script>

	</body>
	
</html>

