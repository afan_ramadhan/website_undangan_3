<?php
$mod = (isset($_GET['mod']) ? $_GET['mod']: "");

$levelUser = mysql_query("SELECT id_level FROM user WHERE id = '$_SESSION[id]'");
$ambilLevelUser = mysql_fetch_array($levelUser);

$menuAkses = mysql_query("SELECT menu.nama_menu, hak_akses.r FROM hak_akses LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE id_level = '$ambilLevelUser[id_level]'");
while($ambilMenuAkses = mysql_fetch_array($menuAkses))
{
	$view[$ambilMenuAkses['nama_menu']] = $ambilMenuAkses['r'];
}
?>

<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<?php
				if($ambil_data_adminweb['gambar']=="")
				{
					echo "<img class='img-circle' src='images/user_kosong.jpg'/>";
				}
				else
				{
					echo "<img class='img-circle' src='images/user/$ambil_data_adminweb[gambar]'/>";
				}
				?>
			</div>
			<div class="pull-left info">
				<p><?=$ambil_data_adminweb['nama_lengkap'];?></p>
				<small><?=$ambil_data_adminweb['nama_level'];?></small>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header" style="text-align: center;"><?=strtoupper($lihat_konfigurasi['nama_aplikasi']);?> MENU</li>
			<li <?php if(!isset($_GET['mod'])){echo "class='active'";} ?>><a class="loading" href="index.php"><i class="fa fa-dashboard" style="margin-right: 10px;"></i><span>Dashboard</span></a></li>
			<li class="treeview <?php if($mod == "level" or $mod == "user"){echo "active menu-open";} ?>" style="<?php if($view['level'] == 0 and $view['user'] == 0){echo "display: none;";} ?>">
				<a href="#">
					<i class="fa fa-lock" style="margin-right: 10px;"></i><span>Manajemen Akses</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">	
					<li <?php if($mod == "level"){echo "class='active'";} ?> style="<?php if($view['level'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=level"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Level</span></a></li>
					<li <?php if($mod == "user"){echo "class='active'";} ?> style="<?php if($view['user'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=user"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>User</span></a></li>
				</ul>
			</li>
			<li <?php if($mod == "pesan"){echo "class='active'";} ?> style="<?php if($view['pesan'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=pesan"><i class="fa fa-envelope-o" style="margin-right: 10px;"></i><span>Pesan</span></a></li>
			<li <?php if($mod == "saran"){echo "class='active'";} ?> style="<?php if($view['saran'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=saran"><i class="fa fa-commenting-o" style="margin-right: 10px;"></i><span>Saran</span></a></li>
			<li <?php if($mod == "riwayat"){echo "class='active'";} ?> style="<?php if($view['riwayat'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=riwayat"><i class="fa fa-clock-o" style="margin-right: 10px;"></i><span>Riwayat Aktivitas</span></a></li>
		</ul>
	</section>
</aside>
