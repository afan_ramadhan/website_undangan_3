<?php
date_default_timezone_set('Asia/Jakarta');

$tanggal_sekarang = date("Y-m-d");

$jam_sekarang = date("H:i:s");

function tanggal($tanggal)
{
	$split = explode("-", $tanggal);
	
	return $split[2] . "-" . $split[1] . "-" . $split[0];
}

function tanggal_indonesia($tanggal)
{
	$bulan = array (1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$split = explode("-", $tanggal);
	
	return $split[2] . " " . $bulan[(int)$split[1]] . " " . $split[0];
}
?>
