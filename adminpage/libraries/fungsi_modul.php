<?php
if(isset($_GET['mod']))
{
	switch($_GET['mod'])
	{
		case '404':
			include("module/404/404.php");
			break;
		case 'level':
			include("module/level/level.php");
			break;
		case 'user':
			include("module/user/user.php");
			break;
		case 'profil':
			include("module/profil/profil.php");
			break;
		case 'konfigurasi':
			include("module/konfigurasi/konfigurasi.php");
			break;
		case 'riwayat':
			include("module/riwayat/riwayat.php");
			break;
		case 'pesan':
			include("module/pesan/pesan.php");
			break;
		case 'saran':
			include("module/saran/saran.php");
			break;
		default:
			include ("module/dashboard/dashboard.php");
			break;
	}   
}
else
{
	include ("module/dashboard/dashboard.php");
}
?>	