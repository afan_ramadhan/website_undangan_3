<?php
if($_POST['mod']=="editAkses")
{
	include "../../config/database.php";
			
	$id_level = $_POST['id'];
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		Edit Akses
	</h4>
</div>
<div class="modal-body">
	<form id="formAkses">
		<table class="table table-hover">
			<tr>
				<td style="border: none;">
					<label class="control-label">Menu</label>
				</td>
				<td style="width: 1px; border: none;"></td>
				<td align="center" style="border: none;">
					<label class="control-label">R</label>
				</td>
				<td align="center" style="border: none;">
					<label class="control-label">S</label>
				</td>
				<td align="center" style="border: none;">
					<label class="control-label">W</label>
				</td>
				<td align="center" style="border: none;">
					<label class="control-label">U</label>
				</td>
				<td align="center" style="border: none;">
					<label class="control-label">D</label>
				</td>
			</tr>
			
			<input type="hidden" name="mod" value="simpanAkses">
			<input type="hidden" name="id_level" value="<?=$id_level;?>">
			
			<?php
			$menu = mysql_query("SELECT * FROM menu ORDER BY no_urut");
			while($getMenu = mysql_fetch_array($menu))
			{
				$HakAkses = mysql_query("SELECT * FROM hak_akses WHERE id_level = '$id_level' AND id_menu = '$getMenu[id]'");
				$getHakAkses = mysql_fetch_array($HakAkses);
			?>
				
				<input type="hidden" name="id_menu[]" value="<?=$getMenu['id'];?>">
				
				<tr>
					<td style="border: none;"><?=$getMenu['keterangan'];?></td>
					<td style="border: none;">:</td>
					<td align="center" style="border: none;">
						<input type="checkbox" name="r[<?=$getMenu['id'];?>]" value="1" <?php if($getHakAkses['r'] == 1){echo "checked";} ?>/>
					</td>
					<td align="center" style="border: none;">
						<input type="checkbox" name="s[<?=$getMenu['id'];?>]" value="1" style="<?php if($getMenu['nama_menu']=="level" or $getMenu['nama_menu']=="user" or $getMenu['nama_menu']=="pesan"){echo "display: none;";} ?>" <?php if($getHakAkses['s'] == 1){echo "checked";} ?>/>
					</td>
					<td align="center" style="border: none;">
						<input type="checkbox" name="w[<?=$getMenu['id'];?>]" value="1" style="<?php if($getMenu['nama_menu']=="riwayat" or $getMenu['nama_menu']=="pesan"){echo "display: none;";} ?>" <?php if($getHakAkses['w'] == 1){echo "checked";} ?>/>
					</td>
					<td align="center" style="border: none;">
						<input type="checkbox" name="u[<?=$getMenu['id'];?>]" value="1" style="<?php if($getMenu['nama_menu']=="riwayat" or $getMenu['nama_menu']=="pesan"){echo "display: none;";} ?>" <?php if($getHakAkses['u'] == 1){echo "checked";} ?>/>
					</td>
					<td align="center" style="border: none;">
						<input type="checkbox" name="d[<?=$getMenu['id'];?>]" value="1" <?php if($getHakAkses['d'] == 1){echo "checked";} ?>/>
					</td>
				</tr>
			
			<?php
			}
			?>
			
		</table>
	</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="simpanAkses" onclick="simpanAkses()"><i class="fa fa-save" aria-hidden="true" style="margin-right: 10px;"></i>Simpan</button>
</div>