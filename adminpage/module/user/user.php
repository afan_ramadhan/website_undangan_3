<?php
$nama_menu = "user";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | User";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$("#tampilData").load("module/user/user_database.php");
		})
		
		//Tambah Data
		function tambahData()
		{
			var mod = "tambahData";
			$.ajax({
				type	: "POST",
				url		: "module/user/user_form.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Simpan Data
		function simpanData()
		{
			mulaiAnimasi();
			var mod = "simpanData";
			var nama_lengkap = $("#nama_lengkap").val();
			var username = $("#username").val();
			var password = $("#password").val();
			var jenis_kelamin = $("#jenis_kelamin").val();
			var tempat_lahir = $("#tempat_lahir").val();
			var tanggal_lahir = $("#tanggal_lahir").val();
			var agama = $("#agama").val();
			var alamat = $("#alamat").val();
			var email = $("#email").val();
			var nomor_telepon = $("#nomor_telepon").val();
			var id_level = $("#id_level").val();
			$.ajax({
				type	: "POST",
				url		: "module/user/user_action.php",
				data	: "mod=" + mod +
						  "&nama_lengkap=" + nama_lengkap +
						  "&username=" + username +
						  "&password=" + password +
						  "&jenis_kelamin=" + jenis_kelamin +
						  "&tempat_lahir=" + tempat_lahir +
						  "&tanggal_lahir=" + tanggal_lahir +
						  "&agama=" + agama +
						  "&alamat=" + alamat +
						  "&email=" + email +
						  "&nomor_telepon=" + nomor_telepon +
						  "&id_level=" + id_level,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/user/user_database.php");
				}
			})
		}
		
		//Edit Data
		function editData(id)
		{
			var mod = "editData";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/user/user_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Perbarui Data
		function perbaruiData(id)
		{
			mulaiAnimasi();
			var mod = "perbaruiData";
			var id = id;
			var nama_lengkap = $("#nama_lengkap").val();
			var username = $("#username").val();
			var password = $("#password").val();
			var jenis_kelamin = $("#jenis_kelamin").val();
			var tempat_lahir = $("#tempat_lahir").val();
			var tanggal_lahir = $("#tanggal_lahir").val();
			var agama = $("#agama").val();
			var alamat = $("#alamat").val();
			var email = $("#email").val();
			var nomor_telepon = $("#nomor_telepon").val();
			var id_level = $("#id_level").val();
			var blokir = $("input[name=blokir]:checked").val();
			$.ajax({
				type	: "POST",
				url		: "module/user/user_action.php",
				data	: "mod=" + mod +
						  "&id=" + id +
						  "&nama_lengkap=" + nama_lengkap +
						  "&username=" + username +
						  "&password=" + password +
						  "&jenis_kelamin=" + jenis_kelamin +
						  "&tempat_lahir=" + tempat_lahir +
						  "&tanggal_lahir=" + tanggal_lahir +
						  "&agama=" + agama +
						  "&alamat=" + alamat +
						  "&email=" + email +
						  "&nomor_telepon=" + nomor_telepon +
						  "&id_level=" + id_level +
						  "&blokir=" + blokir,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/user/user_database.php");
				}
			})
		}
		
		//Hapus Data
		function hapusData(id)
		{
			var konfirmasi = confirm("Hapus Data?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusData";
				var id = id;
				$.ajax({
					type	: "POST",
					url		: "module/user/user_action.php",
					data	: "mod=" + mod +
							  "&id=" + id,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/user/user_database.php");
					}
				})
			}
		}
		
		//Hapus Data Terpilih
		function hapusDataTerpilih()
		{
			var konfirmasi = confirm("Hapus Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/user/user_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/user/user_database.php");
					}
				})
			}
		}
		//Aktifkan Data Terpilih
		function aktifkanDataTerpilih()
		{
			var konfirmasi = confirm("Aktifkan Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "aktifkanDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/user/user_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/user/user_database.php");
					}
				})
			}
		}
		
		//Blokir Data Terpilih
		function blokirDataTerpilih()
		{
			var konfirmasi = confirm("Blokir Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "blokirDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/user/user_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/user/user_database.php");
					}
				})
			}
		}
		
		//Lihat Detail
		function lihatDetail(id)
		{
			var mod = "lihatDetail";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/user/user_form_detail.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent_").html(html);
					$("#form_").modal();
				}
			})
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>User</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">User</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data User</h3>
						</div>
						<div class="box-body">
							<button type="button" class="btn btn-primary btn-md" onclick="tambahData()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Tambah User</button>
							<button type="button" class="btn btn-default" style="margin-right: 10px; <?=$d;?>">
								<input type="checkbox" id="pilihSemua" style="margin-right: 10px;">Pilih Semua
							</button>
							<button type="button" id="hapusDataTerpilih" class="btn btn-danger" onclick="hapusDataTerpilih()" style="<?=$d;?>">
								<i class="fa fa-trash" aria-hidden="true" style="margin-right: 10px;"></i>Hapus User Terpilih</span>
							</button>
							<br style="margin-bottom: 20px; <?=$u;?>"/>
							<button type="button" id="aktifkanDataTerpilih" class="btn btn-success" onclick="aktifkanDataTerpilih()" style="margin-right: 10px; <?=$u;?>">
								<i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;"></i>Aktifkan User Terpilih</span>
							</button>
							<button type="button" id="blokirDataTerpilih" class="btn btn-warning" onclick="blokirDataTerpilih()" style="margin-right: 10px; <?=$u;?>">
								<i class="fa fa-close" aria-hidden="true" style="margin-right: 10px;"></i>Blokir User Terpilih</span>
							</button>
							<div class="modal fade" id="form" role="dialog">
								<div class="modal-dialog">
									<div id="formContent" class="modal-content"></div>
									<div id="notifikasi" class="modal-content"></div>
								</div>
							</div>	
							<div class="modal fade" id="form_" role="dialog">
								<div class="modal-dialog">
									<div id="formContent_" class="modal-content"></div>
								</div>
							</div>			
							<br/>
							<br/>  
							<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>User</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">User</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>